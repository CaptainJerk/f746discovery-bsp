/******************************************************************************
 * File:        lv_port.c
 * Author:      captain 
 * Date:        14.04.2020
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#include "ch.h"
#include "hal.h"
#include "hal_stm32_ltdc.h"
#include "ft5x06.h"
#include "lvgl.h"

static ft5x06_drv_t * ctpd;

static void disp_flush( lv_disp_drv_t * drv, 
                        const lv_area_t * area, 
                        lv_color_t * color_p )
{
    (void) area;
    chSysLock();
    /* Swap display buffers. */
    ltdcBgSetFrameAddressI(&LTDCD1, color_p);
    ltdcStartReloadI(&LTDCD1, false);
    /* Notify lvgl driver. */
    lv_disp_flush_ready(drv);
    chSysUnlock();
}

static bool read_touch_0(lv_indev_drv_t * drv, lv_indev_data_t * d)
{
    (void) drv;
    ft5x06_update(ctpd);
    d->state = ft5x06_get_channel(ctpd, 0, &d->point.y, &d->point.x) == 2 ?
                LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}

static bool read_touch_1(lv_indev_drv_t * drv, lv_indev_data_t * d)
{
    (void) drv;
    d->state = ft5x06_get_channel(ctpd, 1, &d->point.y, &d->point.x) == 2 ?
                LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}

static bool read_touch_2(lv_indev_drv_t * drv, lv_indev_data_t * d)
{
    (void) drv;
    d->state = ft5x06_get_channel(ctpd, 2, &d->point.y, &d->point.x) == 2 ?
                LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}

static bool read_touch_3(lv_indev_drv_t * drv, lv_indev_data_t * d)
{
    (void) drv;
    d->state = ft5x06_get_channel(ctpd, 3, &d->point.y, &d->point.x) == 2 ?
                LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}

static bool read_touch_4(lv_indev_drv_t * drv, lv_indev_data_t * d)
{
    (void) drv;
    d->state = ft5x06_get_channel(ctpd, 4, &d->point.y, &d->point.x) == 2 ?
                LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}

static void indev_init(void)
{
    static FT5x06_DRV_OBJECT(ctp_drv_pool);

    /* Touchscreen configuration */
    ctpd = ft5x06_object_init(ctp_drv_pool, &I2CD3);
    ft5x06_start(ctpd, NULL);

    lv_indev_drv_t t0, t1, t2, t3, t4;
    lv_indev_drv_init(&t0);
    lv_indev_drv_init(&t1);
    lv_indev_drv_init(&t2);
    lv_indev_drv_init(&t3);
    lv_indev_drv_init(&t4);

    t0.type = LV_INDEV_TYPE_POINTER;
    t0.read_cb = read_touch_0;
    t1.type = LV_INDEV_TYPE_POINTER;
    t1.read_cb = read_touch_1;
    t2.type = LV_INDEV_TYPE_POINTER;
    t2.read_cb = read_touch_2;
    t3.type = LV_INDEV_TYPE_POINTER;
    t3.read_cb = read_touch_3;
    t4.type = LV_INDEV_TYPE_POINTER;
    t4.read_cb = read_touch_4;

    lv_indev_drv_register(&t0);
    lv_indev_drv_register(&t1);
    lv_indev_drv_register(&t2);
    lv_indev_drv_register(&t3);
    lv_indev_drv_register(&t4);
}

static void disp_init(void)
{
    static ltdc_frame_t bg_frame = {
        .width          = LCD_SCREEN_W,
        .height         = LCD_SCREEN_H,
        .pitch          = LCD_SCREEN_W * sizeof(ltdc_color_t),
        .fmt            = LTDC_FMT_ARGB8888
    };

    /* Full screen window. */
    static const ltdc_window_t win = {
        .hstart         = 0,
        .hstop          = LCD_SCREEN_W - 1,
        .vstart         = 0,
        .vstop          = LCD_SCREEN_H - 1
    };

    static const ltdc_laycfg_t bg = {
        .frame          = &bg_frame,
        .window         = &win,
        .def_color      = LTDC_COLOR_BLACK,
        .const_alpha    = 0xff,
        .key_color      = LTDC_COLOR_LIME,
        .pal_colors     = NULL,
        .pal_length     = 0,
        .blending       = LTDC_BLEND_MOD1_MOD2,
        .flags          = LTDC_LEF_KEYING | LTDC_LEF_ENABLE
    };

    /* LCD configuration */
    static const LTDCConfig lcd_cfg = {
        .bg_laycfg      = NULL,
        .fg_laycfg      = NULL,

        /* IRQ handlers */
        .fuerr_isr      = NULL,
        .line_isr       = NULL,
        .rr_isr         = NULL,
        .terr_isr       = NULL,

        .flags          = 0,
        .clear_color    = LTDC_COLOR_BLACK,

        /* Display timings */
        .screen_width   = LCD_SCREEN_W,
        .screen_height  = LCD_SCREEN_H,

        .vsync_height   = LCD_VSYNC,
        .vbp_height     = LCD_VBP,
        .vfp_height     = LCD_VFP,

        .hsync_width    = LCD_HSYNC,
        .hbp_width      = LCD_HBP,
        .hfp_width      = LCD_HFP
    };
    ltdcInit();
    ltdcStart(&LTDCD1, &lcd_cfg);

    static lv_disp_buf_t disp_buf;

    /* Framebuffer allocation. Use 2 fullscreen buffer configuration. */
    lv_color_t * b = chHeapAlloc(   NULL, 2 * LCD_SCREEN_H * LCD_SCREEN_W \
                                    * sizeof(ltdc_color_t) );
    chDbgAssert(b, "vbuf allocation fail");
    memset(b, 0x00, LCD_SCREEN_H * LCD_SCREEN_W * sizeof(ltdc_color_t));
    bg_frame.bufferp = b + LCD_SCREEN_H * LCD_SCREEN_W;

    lv_disp_buf_init(   &disp_buf, 
                        b, 
                        b + LCD_SCREEN_H * LCD_SCREEN_W,
                        LCD_SCREEN_H * LCD_SCREEN_W );

    /* LCD driver configuration. */
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.hor_res    = LCD_SCREEN_W;
    disp_drv.ver_res    = LCD_SCREEN_H;
    disp_drv.buffer     = &disp_buf;
    disp_drv.flush_cb   = disp_flush;
    lv_disp_drv_register(&disp_drv);

    /* Enable GUI LTDC layer and turn on the backlight. */
    ltdcBgSetConfig(&LTDCD1, &bg);
    lcd_enable_backlight();
}

static thread_reference_t lvgl_trp = NULL;

static THD_WORKING_AREA(wa_lv_thread, 2048);
static THD_FUNCTION(lv_thread, arg)
{
    (void) arg;
    static const unsigned dt = 10;

    chRegSetThreadName("lvgl");

    /* lvgl library initialization. */
    lv_init();

    disp_init();
    indev_init();

    chThdResume(&lvgl_trp, MSG_OK);
    while (true) {
        chThdSleepMilliseconds(dt);
        /* Periodic task. */
        lv_tick_inc(dt);
        lv_task_handler();
    }
}

void lvglInit(void)
{
    chThdCreateStatic(  wa_lv_thread, sizeof(wa_lv_thread), 
                        LOWPRIO, lv_thread, NULL );
    chSysLock();
    chThdSuspendS(&lvgl_trp);
    chSysUnlock();
}

void * lv_port_alloc(size_t size)
{
    return chHeapAlloc(NULL, size);
}

void lv_port_free(void * ptr)
{
    chHeapFree(ptr);
}

/******************************** End of file ********************************/

