/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS, 
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * This file has been automatically generated using ChibiStudio board
 * generator plugin. Do not edit manually.
 */

#include "hal.h"

/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported variables.                                                */
/*===========================================================================*/

/*===========================================================================*/
/* Driver local variables and types.                                         */
/*===========================================================================*/

/**
 * @brief   Type of STM32 GPIO port setup.
 */
typedef struct {
  uint32_t              moder;
  uint32_t              otyper; uint32_t              ospeedr;
  uint32_t              pupdr;
  uint32_t              odr;
  uint32_t              afrl;
  uint32_t              afrh;
} gpio_setup_t;

/**
 * @brief   Type of STM32 GPIO initialization data.
 */
typedef struct {
#if STM32_HAS_GPIOA || defined(__DOXYGEN__)
  gpio_setup_t          PAData;
#endif
#if STM32_HAS_GPIOB || defined(__DOXYGEN__)
  gpio_setup_t          PBData;
#endif
#if STM32_HAS_GPIOC || defined(__DOXYGEN__)
  gpio_setup_t          PCData;
#endif
#if STM32_HAS_GPIOD || defined(__DOXYGEN__)
  gpio_setup_t          PDData;
#endif
#if STM32_HAS_GPIOE || defined(__DOXYGEN__)
  gpio_setup_t          PEData;
#endif
#if STM32_HAS_GPIOF || defined(__DOXYGEN__)
  gpio_setup_t          PFData;
#endif
#if STM32_HAS_GPIOG || defined(__DOXYGEN__)
  gpio_setup_t          PGData;
#endif
#if STM32_HAS_GPIOH || defined(__DOXYGEN__)
  gpio_setup_t          PHData;
#endif
#if STM32_HAS_GPIOI || defined(__DOXYGEN__)
  gpio_setup_t          PIData;
#endif
#if STM32_HAS_GPIOJ || defined(__DOXYGEN__)
  gpio_setup_t          PJData;
#endif
#if STM32_HAS_GPIOK || defined(__DOXYGEN__)
  gpio_setup_t          PKData;
#endif
} gpio_config_t;

/**
 * @brief   STM32 GPIO static initialization data.
 */
static const gpio_config_t gpio_default_config = {
#if STM32_HAS_GPIOA
  {VAL_GPIOA_MODER, VAL_GPIOA_OTYPER, VAL_GPIOA_OSPEEDR, VAL_GPIOA_PUPDR,
   VAL_GPIOA_ODR,   VAL_GPIOA_AFRL,   VAL_GPIOA_AFRH},
#endif
#if STM32_HAS_GPIOB
  {VAL_GPIOB_MODER, VAL_GPIOB_OTYPER, VAL_GPIOB_OSPEEDR, VAL_GPIOB_PUPDR,
   VAL_GPIOB_ODR,   VAL_GPIOB_AFRL,   VAL_GPIOB_AFRH},
#endif
#if STM32_HAS_GPIOC
  {VAL_GPIOC_MODER, VAL_GPIOC_OTYPER, VAL_GPIOC_OSPEEDR, VAL_GPIOC_PUPDR,
   VAL_GPIOC_ODR,   VAL_GPIOC_AFRL,   VAL_GPIOC_AFRH},
#endif
#if STM32_HAS_GPIOD
  {VAL_GPIOD_MODER, VAL_GPIOD_OTYPER, VAL_GPIOD_OSPEEDR, VAL_GPIOD_PUPDR,
   VAL_GPIOD_ODR,   VAL_GPIOD_AFRL,   VAL_GPIOD_AFRH},
#endif
#if STM32_HAS_GPIOE
  {VAL_GPIOE_MODER, VAL_GPIOE_OTYPER, VAL_GPIOE_OSPEEDR, VAL_GPIOE_PUPDR,
   VAL_GPIOE_ODR,   VAL_GPIOE_AFRL,   VAL_GPIOE_AFRH},
#endif
#if STM32_HAS_GPIOF
  {VAL_GPIOF_MODER, VAL_GPIOF_OTYPER, VAL_GPIOF_OSPEEDR, VAL_GPIOF_PUPDR,
   VAL_GPIOF_ODR,   VAL_GPIOF_AFRL,   VAL_GPIOF_AFRH},
#endif
#if STM32_HAS_GPIOG
  {VAL_GPIOG_MODER, VAL_GPIOG_OTYPER, VAL_GPIOG_OSPEEDR, VAL_GPIOG_PUPDR,
   VAL_GPIOG_ODR,   VAL_GPIOG_AFRL,   VAL_GPIOG_AFRH},
#endif
#if STM32_HAS_GPIOH
  {VAL_GPIOH_MODER, VAL_GPIOH_OTYPER, VAL_GPIOH_OSPEEDR, VAL_GPIOH_PUPDR,
   VAL_GPIOH_ODR,   VAL_GPIOH_AFRL,   VAL_GPIOH_AFRH},
#endif
#if STM32_HAS_GPIOI
  {VAL_GPIOI_MODER, VAL_GPIOI_OTYPER, VAL_GPIOI_OSPEEDR, VAL_GPIOI_PUPDR,
   VAL_GPIOI_ODR,   VAL_GPIOI_AFRL,   VAL_GPIOI_AFRH},
#endif
#if STM32_HAS_GPIOJ
  {VAL_GPIOJ_MODER, VAL_GPIOJ_OTYPER, VAL_GPIOJ_OSPEEDR, VAL_GPIOJ_PUPDR,
   VAL_GPIOJ_ODR,   VAL_GPIOJ_AFRL,   VAL_GPIOJ_AFRH},
#endif
#if STM32_HAS_GPIOK
  {VAL_GPIOK_MODER, VAL_GPIOK_OTYPER, VAL_GPIOK_OSPEEDR, VAL_GPIOK_PUPDR,
   VAL_GPIOK_ODR,   VAL_GPIOK_AFRL,   VAL_GPIOK_AFRH}
#endif
};

/*===========================================================================*/
/* Driver local functions.                                                   */
/*===========================================================================*/

static void gpio_init(stm32_gpio_t *gpiop, const gpio_setup_t *config) {

  gpiop->OTYPER  = config->otyper;
  gpiop->OSPEEDR = config->ospeedr;
  gpiop->PUPDR   = config->pupdr;
  gpiop->ODR     = config->odr;
  gpiop->AFRL    = config->afrl;
  gpiop->AFRH    = config->afrh;
  gpiop->MODER   = config->moder;
}

static void stm32_gpio_init(void) {

  /* Enabling GPIO-related clocks, the mask comes from the
     registry header file.*/
  rccResetAHB1(STM32_GPIO_EN_MASK);
  rccEnableAHB1(STM32_GPIO_EN_MASK, true);

  /* Initializing all the defined GPIO ports.*/
#if STM32_HAS_GPIOA
  gpio_init(GPIOA, &gpio_default_config.PAData);
#endif
#if STM32_HAS_GPIOB
  gpio_init(GPIOB, &gpio_default_config.PBData);
#endif
#if STM32_HAS_GPIOC
  gpio_init(GPIOC, &gpio_default_config.PCData);
#endif
#if STM32_HAS_GPIOD
  gpio_init(GPIOD, &gpio_default_config.PDData);
#endif
#if STM32_HAS_GPIOE
  gpio_init(GPIOE, &gpio_default_config.PEData);
#endif
#if STM32_HAS_GPIOF
  gpio_init(GPIOF, &gpio_default_config.PFData);
#endif
#if STM32_HAS_GPIOG
  gpio_init(GPIOG, &gpio_default_config.PGData);
#endif
#if STM32_HAS_GPIOH
  gpio_init(GPIOH, &gpio_default_config.PHData);
#endif
#if STM32_HAS_GPIOI
  gpio_init(GPIOI, &gpio_default_config.PIData);
#endif
#if STM32_HAS_GPIOJ
  gpio_init(GPIOJ, &gpio_default_config.PJData);
#endif
#if STM32_HAS_GPIOK
  gpio_init(GPIOK, &gpio_default_config.PKData);
#endif
}

/*===========================================================================*/
/* Driver interrupt handlers.                                                */
/*===========================================================================*/

/*===========================================================================*/
/* Driver exported functions.                                                */
/*===========================================================================*/

/**
 * @brief   Early initialization code.
 * @details GPIO ports and system clocks are initialized before everything
 *          else.
 */
void __early_init(void) {

  stm32_gpio_init();
  stm32_clock_init();
}

#if HAL_USE_SDC || defined(__DOXYGEN__)
/**
 * @brief   SDC card detection.
 */
bool sdc_lld_is_card_inserted(SDCDriver *sdcp) {

  (void)sdcp;

  return !palReadPad(GPIOC, GPIOC_SD_DETECT);
}

/**
 * @brief   SDC card write protection detection.
 */
bool sdc_lld_is_write_protected(SDCDriver *sdcp) {

  (void)sdcp;
  /* CHTODO: Fill the implementation.*/
  return false;
}
#endif /* HAL_USE_SDC */

#if HAL_USE_MMC_SPI || defined(__DOXYGEN__)
/**
 * @brief   MMC_SPI card detection.
 */
bool mmc_lld_is_card_inserted(MMCDriver *mmcp) {

  (void)mmcp;
  /* CHTODO: Fill the implementation.*/
  return true;
}

/**
 * @brief   MMC_SPI card write protection detection.
 */
bool mmc_lld_is_write_protected(MMCDriver *mmcp) {

  (void)mmcp;
  /* CHTODO: Fill the implementation.*/
  return false;
}
#endif

void lcd_enable_backlight(void)
{
    palSetLine(LINE_LCD_BL_CTRL);
}

void lcd_disable_backlight(void)
{
    palClearLine(LINE_LCD_BL_CTRL);
}

void qspi_flash_init(void)
{
    static const WSPIConfig wspi_conf = {
        .dcr        =   ((QSPI_FLASH_SIZE_LOG2 - 1) << QUADSPI_DCR_FSIZE_Pos) |
                        ((QSPI_FLASH_CS_HI_TIME - 1) << QUADSPI_DCR_CSHT_Pos),
        .end_cb     =   NULL,
        .error_cb   =   NULL
    };

    static const wspi_command_t cmd = {
        .cfg        =   WSPI_CFG_CMD_MODE_ONE_LINE |
                        WSPI_CFG_DATA_MODE_FOUR_LINES |
                        WSPI_CFG_ADDR_MODE_FOUR_LINES |
                        WSPI_CFG_ADDR_SIZE_24,
        .cmd        =   0xeb, // QUAD INPUT/OUTPUT FAST READ
        .addr       =   0,
        .alt        =   0,
        .dummy      =   QSPI_FLASH_DUMMY
    };
    wspiStart(&WSPID1, &wspi_conf);
    WSPID1.qspi->CR |= QUADSPI_CR_SSHIFT;
    wspiMapFlash(&WSPID1, &cmd, NULL);
}

/**
 * @brief   SDRAM initialization routine.
 */
void sdram_init(void)
{
    /* SDRAM configuration */
    static const SDRAMConfig sdram_cfg = {
        .sdcmr  =   FMC_SDCMR_MRD_CAS_LATENCY_2 |
                    FMC_SDCMR_MRD_BURST_LENGTH_8 |
                    FMC_SDCMR_MRD_BURST_TYPE_SEQUENTIAL |
                    FMC_SDCMR_MRD_WRITEBURST_MODE_PROGRAMMED |
                    (15 << FMC_SDCMR_NRFS_Pos),
        .sdcr   =   (2 << FMC_SDCR1_SDCLK_Pos) |    // HCLK / 2
                    (2 << FMC_SDCR1_CAS_Pos) |      // CAS 2
                    FMC_SDCR1_NB |                  // 4 internal banks
                    FMC_SDCR1_MWID_0 |              // 16-bit access
                    (1 << FMC_SDCR1_NR_Pos) |       // 12 row lines
                    (0 << FMC_SDCR1_NC_Pos),        // 8 col lines
        .sdrtr  =   SDRAM_NS2TICKS(SDRAM_REF_PERIOD) << FMC_SDRTR_COUNT_Pos,
        .sdtr   =   SDRAM_NS2TICKS(SDRAM_TRC) << FMC_SDTR1_TRC_Pos |
                    SDRAM_NS2TICKS(SDRAM_TRP) << FMC_SDTR1_TRP_Pos |
                    SDRAM_NS2TICKS(SDRAM_TWR) << FMC_SDTR1_TWR_Pos |
                    SDRAM_NS2TICKS(SDRAM_TRCD) << FMC_SDTR1_TRCD_Pos |
                    SDRAM_NS2TICKS(SDRAM_TMRD) << FMC_SDTR1_TMRD_Pos |
                    SDRAM_NS2TICKS(SDRAM_TXSR) << FMC_SDTR1_TXSR_Pos |
                    SDRAM_NS2TICKS(SDRAM_TRAS) << FMC_SDTR1_TRAS_Pos
    };
    sdramObjectInit(&SDRAMD1);
    sdramStart(&SDRAMD1, &sdram_cfg);
}

void i2c_bus_init(void)
{
    /* I2C3 bus configuration */
    static const unsigned I2C_PCLK  = STM32_I2C3CLK / 16;
    static const unsigned I2C_FSCL  = 100000; /// Hz
    static const unsigned TR_SC     = ((I2C_PCLK + 2 * I2C_FSCL - 1) / \
                                        (2 * I2C_FSCL) - 1);
    static const I2CConfig i2c_cfg = {
        .timingr    =   STM32_TIMINGR_PRESC(15) |
                        STM32_TIMINGR_SCLDEL(1) |
                        STM32_TIMINGR_SDADEL(0) |
                        STM32_TIMINGR_SCLH(TR_SC) |
                        STM32_TIMINGR_SCLL(TR_SC),
        .cr1        = 0,
        .cr2        = 0
    };
    i2cStart(&I2CD3, &i2c_cfg);
}

__attribute__((weak))
void syshalt_hook(const char * reason)
{
    (void) reason;
}

/**
 * @brief   Board-specific initialization code.
 * @note    You can add your board-specific code here.
 */
void boardInit(void)
{
    mpuDisable();
}

