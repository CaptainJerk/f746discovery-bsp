/******************************************************************************
 * File:        lv_conf.h
 * Author:      captain 
 * Date:        14.04.2020
 * Description: lvgl configuration file.
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#ifndef LV_CONF_H
#define LV_CONF_H

#include <stdint.h>
#include <stddef.h>

/*====================
   Graphical settings
 *====================*/

/* Maximal horizontal and vertical resolution to support by the library.*/
#define LV_HOR_RES_MAX                      480
#define LV_VER_RES_MAX                      272

/* Color depth: ARGB8888 */
#define LV_COLOR_DEPTH                      32

#define LV_COLOR_16_SWAP                    0
#define LV_COLOR_SCREEN_TRANSP              0
#define LV_COLOR_TRANSP                     LV_COLOR_LIME

/* Enable chroma keying for indexed images. */
#define LV_INDEXED_CHROMA                   1

/* Enable anti-aliasing (lines, and radiuses will be smoothed) */
#define LV_ANTIALIAS                        1

/* Default display refresh period. */
#define LV_DISP_DEF_REFR_PERIOD             30 /* ms */

/* Dot Per Inch: used to initialize default sizes. */
#define LV_DPI                              100 /* px */

typedef int16_t lv_coord_t;

/*=========================
   Memory manager settings
 *=========================*/

/* LittelvGL's internal memory manager's settings.
 * The graphical objects and other related data are stored here. */
extern void * lv_port_alloc(size_t);
extern void lv_port_free(void *);
#define LV_MEM_CUSTOM                       1
#define LV_MEM_CUSTOM_ALLOC                 lv_port_alloc
#define LV_MEM_CUSTOM_FREE                  lv_port_free

/* Garbage Collector settings */
#define LV_ENABLE_GC                        0

/*=======================
   Input device settings
 *=======================*/

/* Input device read period in milliseconds */
#define LV_INDEV_DEF_READ_PERIOD            30

/* Drag threshold in pixels */
#define LV_INDEV_DEF_DRAG_LIMIT             10

/* Drag throw slow-down in [%]. Greater value -> faster slow-down */
#define LV_INDEV_DEF_DRAG_THROW             20

/* Long press time in milliseconds.
 * Time to send `LV_EVENT_LONG_PRESSSED`) */
#define LV_INDEV_DEF_LONG_PRESS_TIME        400

/* Repeated trigger period in long press [ms]
 * Time between `LV_EVENT_LONG_PRESSED_REPEAT */
#define LV_INDEV_DEF_LONG_PRESS_REP_TIME    100

/*==================
 * Feature usage
 *==================*/

/* Enable the Animations */
#define LV_USE_ANIMATION                    1

/* Enable shadow drawing*/
#define LV_USE_SHADOW                       1

/* Enable object groups (for keyboard/encoder navigation) */
#define LV_USE_GROUP                        1

/* Enable GPU interface*/
#define LV_USE_GPU                          0

/* Enable file system (might be required for images */
#define LV_USE_FILESYSTEM                   0

/* Add a `user_data` to drivers and objects*/
#define LV_USE_USER_DATA                    0

typedef void * lv_anim_user_data_t;
typedef void * lv_group_user_data_t;
typedef void * lv_fs_drv_user_data_t;

/*========================
 * Image decoder and cache
 *========================*/

/* Enable indexed (palette) images */
#define LV_IMG_CF_INDEXED                   1

/* Enable alpha indexed images */
#define LV_IMG_CF_ALPHA                     1

/* Default image cache size. Image caching keeps the images opened.
 * If only the built-in image formats are used there is no real advantage of caching.
 * (I.e. no new image decoder is added)
 * With complex image decoders (e.g. PNG or JPG) caching can save the continuous open/decode of images.
 * However the opened images might consume additional RAM.
 * LV_IMG_CACHE_DEF_SIZE must be >= 1 */
#define LV_IMG_CACHE_DEF_SIZE               1

/*Declare the type of the user data of image decoder (can be e.g. `void *`, `int`, `struct`)*/
typedef void * lv_img_decoder_user_data_t;

/*=====================
 *  Compiler settings
 *====================*/

#define LV_ATTRIBUTE_TICK_INC
#define LV_ATTRIBUTE_TASK_HANDLER
#define LV_ATTRIBUTE_MEM_ALIGN              __attribute__((aligned(4)))
#define LV_ATTRIBUTE_LARGE_CONST
#define LV_EXPORT_CONST_INT(int_value)      struct _silence_gcc_warning

/*===================
 *  HAL settings
 *==================*/

/* It removes the need to manually update the tick with `lv_tick_inc`) */
#define LV_TICK_CUSTOM                      0

typedef void * lv_disp_drv_user_data_t;
typedef void * lv_indev_drv_user_data_t;

/*================
 * Log settings
 *===============*/

#define LV_USE_LOG                          0

/*=================
 * Debug settings
 *================*/

/* If Debug is enabled LittelvGL validates the parameters of the functions.
 * If an invalid parameter is found an error log message is printed and
 * the MCU halts at the error. (`LV_USE_LOG` should be enabled)
 * If you are debugging the MCU you can pause
 * the debugger to see exactly where  the issue is.
 *
 * The behavior of asserts can be overwritten by redefining them here.
 * E.g. #define LV_ASSERT_MEM(p)  <my_assert_code>
 */
#define LV_USE_DEBUG                        1
#if LV_USE_DEBUG

/* Check if the parameter is NULL. (Quite fast) */
#define LV_USE_ASSERT_NULL                  1

/* Checks is the memory is successfully allocated or no. (Quite fast)*/
#define LV_USE_ASSERT_MEM                   1

/* Check the strings.
 * Search for NULL, very long strings, invalid characters, 
 * and unnatural repetitions. (Slow)
 * If disabled `LV_USE_ASSERT_NULL` will be performed instead 
 * (if it's enabled) */
#define LV_USE_ASSERT_STR                   0

/* Check NULL, the object's type and existence (e.g. not deleted). (Quite slow)
 * If disabled `LV_USE_ASSERT_NULL` will be performed instead
 * (if it's enabled) */
#define LV_USE_ASSERT_OBJ                   0

/* Check if the styles are properly initialized. (Fast)*/
#define LV_USE_ASSERT_STYLE                 1

#endif /*LV_USE_DEBUG*/

/*================
 *  THEME USAGE
 *================*/
#define LV_THEME_LIVE_UPDATE                0

#define LV_USE_THEME_TEMPL                  0
#define LV_USE_THEME_DEFAULT                1
#define LV_USE_THEME_ALIEN                  1
#define LV_USE_THEME_NIGHT                  1
#define LV_USE_THEME_MONO                   0
#define LV_USE_THEME_MATERIAL               1
#define LV_USE_THEME_ZEN                    1
#define LV_USE_THEME_NEMO                   0

/*==================
 *    FONT USAGE
 *===================*/

/* Robot fonts with bpp = 4
 * https://fonts.google.com/specimen/Roboto  */
#define LV_FONT_ROBOTO_12                   0
#define LV_FONT_ROBOTO_16                   1
#define LV_FONT_ROBOTO_22                   0
#define LV_FONT_ROBOTO_28                   0

/* Demonstrate special features */
#define LV_FONT_ROBOTO_12_SUBPX             1
#define LV_FONT_ROBOTO_28_COMPRESSED        1

/* Pixel perfect monospace font
 * http://pelulamu.net/unscii/ */
#define LV_FONT_UNSCII_8                    0

/* Optionally declare your custom fonts here.
 * You can use these fonts as default font too
 * and they will be available globally. E.g.
 * #define LV_FONT_CUSTOM_DECLARE LV_FONT_DECLARE(my_font_1) \
 *                                LV_FONT_DECLARE(my_font_2)
 */
#define LV_FONT_CUSTOM_DECLARE

/* Always set a default font from the built-in fonts*/
#define LV_FONT_DEFAULT                     &lv_font_roboto_16

/* Enable it if you have fonts with a lot of characters.
 * The limit depends on the font size, font face and bpp
 * but with > 10,000 characters if you see issues probably you 
 * need to enable it.*/
#define LV_FONT_FMT_TXT_LARGE               0

/* Set the pixel order of the display.
 * Important only if "subpx fonts" are used.
 * With "normal" font it doesn't matter.
 */
#define LV_FONT_SUBPX_BGR                   0

/* Declare the type of the user data of fonts 
 * (can be e.g. `void *`, `int`, `struct`)*/
typedef void * lv_font_user_data_t;

/*=================
 *  Text settings
 *=================*/

/* Select a character encoding for strings.
 * Your IDE or editor should have the same character encoding
 * - LV_TXT_ENC_UTF8
 * - LV_TXT_ENC_ASCII
 * */
#define LV_TXT_ENC LV_TXT_ENC_UTF8

 /*Can break (wrap) texts on these chars*/
#define LV_TXT_BREAK_CHARS                  " ,.;:-_"

/* If a word is at least this long, will break wherever "prettiest"
 * To disable, set to a value <= 0 */
#define LV_TXT_LINE_BREAK_LONG_LEN          12

/* Minimum number of characters in a long word to put on a line before a break.
 * Depends on LV_TXT_LINE_BREAK_LONG_LEN. */
#define LV_TXT_LINE_BREAK_LONG_PRE_MIN_LEN  3

/* Minimum number of characters in a long word to put on a line after a break.
 * Depends on LV_TXT_LINE_BREAK_LONG_LEN. */
#define LV_TXT_LINE_BREAK_LONG_POST_MIN_LEN 3

/* The control character to use for signalling text recoloring. */
#define LV_TXT_COLOR_CMD                    "#"

/* Support bidirectional texts. */
#define LV_USE_BIDI                         0
#if LV_USE_BIDI
/* Set the default direction. Supported values:
 * `LV_BIDI_DIR_LTR` Left-to-Right
 * `LV_BIDI_DIR_RTL` Right-to-Left
 * `LV_BIDI_DIR_AUTO` detect texts base direction */
#define LV_BIDI_BASE_DIR_DEF                LV_BIDI_DIR_AUTO
#endif

/* Change the built in (v)snprintf functions*/
#define LV_SPRINTF_CUSTOM                   0

/*===================
 *  LV_OBJ SETTINGS
 *==================*/

/* Declare the type of the user data of object 
 * (can be e.g. `void *`, `int`, `struct`)*/
typedef void * lv_obj_user_data_t;

/*1: enable `lv_obj_realaign()` based on `lv_obj_align()` parameters*/
#define LV_USE_OBJ_REALIGN                  1

/* Enable to make the object clickable on a larger area.
 * LV_EXT_CLICK_AREA_OFF or 0: Disable this feature
 * LV_EXT_CLICK_AREA_TINY:  The extra area can be adjusted horizontally 
 *                          and vertically (0..255 px)
 * LV_EXT_CLICK_AREA_FULL:  The extra area can be adjusted in all 
 *                          4 directions (-32k..+32k px)
 */
#define LV_USE_EXT_CLICK_AREA               LV_EXT_CLICK_AREA_OFF

/*==================
 *  LV OBJ X USAGE
 *================*/
/*
 * Documentation of the object types: https://docs.littlevgl.com/#Object-types
 */

/* Arc (dependencies: -) */
#define LV_USE_ARC                          1

/* Bar (dependencies: -) */
#define LV_USE_BAR                          1

/* Button (dependencies: lv_cont) */
#define LV_USE_BTN                          1
#if LV_USE_BTN != 0
/* Enable button-state animations -
 * draw a circle on click (dependencies: LV_USE_ANIMATION) */
#define LV_BTN_INK_EFFECT                   0
#endif

/* Button matrix (dependencies: -) */
#define LV_USE_BTNM                         1

/* Calendar (dependencies: -) */
#define LV_USE_CALENDAR                     1

/* Canvas (dependencies: lv_img) */
#define LV_USE_CANVAS                       1

/* Check box (dependencies: lv_btn, lv_label) */
#define LV_USE_CB                           1

/* Chart (dependencies: -) */
#define LV_USE_CHART                        1
#if LV_USE_CHART
#define LV_CHART_AXIS_TICK_LABEL_MAX_LEN    20
#endif

/* Container (dependencies: -) */
#define LV_USE_CONT                         1

/*Color picker (dependencies: -*/
#define LV_USE_CPICKER                      1

/* Drop down list (dependencies: lv_page, lv_label, lv_symbol_def.h) */
#define LV_USE_DDLIST                       1
#if LV_USE_DDLIST != 0
/* Open and close default animation time [ms] (0: no animation) */
#define LV_DDLIST_DEF_ANIM_TIME             200
#endif

/* Gauge (dependencies:lv_bar, lv_lmeter) */
#define LV_USE_GAUGE                        1

/* Image (dependencies: lv_label) */
#define LV_USE_IMG                          1

/*Image Button (dependencies: lv_btn) */
#define LV_USE_IMGBTN                       1
#if LV_USE_IMGBTN
#define LV_IMGBTN_TILED                     0
#endif

/* Keyboard (dependencies: lv_btnm) */
#define LV_USE_KB                           1

/* Label (dependencies: -) */
#define LV_USE_LABEL                        1
#if LV_USE_LABEL != 0
/* Hor, or ver. scroll speed [px/sec] */
#define LV_LABEL_DEF_SCROLL_SPEED           25

/* Waiting period at beginning/end of animation cycle */
#define LV_LABEL_WAIT_CHAR_COUNT            3

/* Enable selecting text of the label */
#define LV_LABEL_TEXT_SEL                   0

/* Store extra some info in labels (12 bytes) to speed up 
 * drawing of very long texts */
#define LV_LABEL_LONG_TXT_HINT              0
#endif

/* LED (dependencies: -) */
#define LV_USE_LED                          1

/* Line (dependencies: -) */
#define LV_USE_LINE                         1

/* List (dependencies: lv_page, lv_btn, lv_label, 
 * (lv_img optionally for icons )) */
#define LV_USE_LIST                         1
#if LV_USE_LIST != 0
/* Default animation time of focusing to a list element [ms] 
 * (0: no animation) */
#define LV_LIST_DEF_ANIM_TIME               100
#endif

/* Line meter (dependencies: *;) */
#define LV_USE_LMETER                       1

/* Message box (dependencies: lv_rect, lv_btnm, lv_label) */
#define LV_USE_MBOX                         1

/* Page (dependencies: lv_cont) */
#define LV_USE_PAGE                         1
#if LV_USE_PAGE != 0
/*Focus default animation time [ms] (0: no animation)*/
#define LV_PAGE_DEF_ANIM_TIME               400
#endif

/* Preload (dependencies: lv_arc, lv_anim) */
#define LV_USE_PRELOAD                      1
#if LV_USE_PRELOAD != 0
#define LV_PRELOAD_DEF_ARC_LENGTH           60      /*[deg]*/
#define LV_PRELOAD_DEF_SPIN_TIME            1000    /*[ms]*/
#define LV_PRELOAD_DEF_ANIM                 LV_PRELOAD_TYPE_SPINNING_ARC
#endif

/* Roller (dependencies: lv_ddlist) */
#define LV_USE_ROLLER                       1
#if LV_USE_ROLLER != 0
/* Focus animation time [ms] (0: no animation) */
#define LV_ROLLER_DEF_ANIM_TIME             200

/* Number of extra "pages" when the roller is infinite */
#define LV_ROLLER_INF_PAGES                 7
#endif

/* Slider (dependencies: lv_bar) */
#define LV_USE_SLIDER                       1

/* Spinbox (dependencies: lv_ta) */
#define LV_USE_SPINBOX                      1

/* Switch (dependencies: lv_slider) */
#define LV_USE_SW                           1

/* Text area (dependencies: lv_label, lv_page) */
#define LV_USE_TA                           1
#if LV_USE_TA != 0
#define LV_TA_DEF_CURSOR_BLINK_TIME         400     /*ms*/
#define LV_TA_DEF_PWD_SHOW_TIME             1500    /*ms*/
#endif

/* Table (dependencies: lv_label) */
#define LV_USE_TABLE                        1
#if LV_USE_TABLE
#define LV_TABLE_COL_MAX                    12
#endif

/* Tab (dependencies: lv_page, lv_btnm) */
#define LV_USE_TABVIEW                      1
#if LV_USE_TABVIEW != 0
/* Time of slide animation [ms] (0: no animation) */
#define LV_TABVIEW_DEF_ANIM_TIME            300
#endif

/* Tileview (dependencies: lv_page) */
#define LV_USE_TILEVIEW                     1
#if LV_USE_TILEVIEW
/* Time of slide animation [ms] (0: no animation) */
#define LV_TILEVIEW_DEF_ANIM_TIME           300
#endif

/* Window (dependencies: lv_cont, lv_btn, lv_label, lv_img, lv_page) */
#define LV_USE_WIN                          1

/* Be sure every define has a default value */
#include "lv_conf_checker.h"

#endif /* LV_CONF_H */

/******************************** End of file ********************************/

