/******************************************************************************
 * File:        platform.h
 * Author:      captain 
 * Date:        15.04.2020
 * Description: Module description
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <ch.h>
#include <hal.h>
#include <lvgl.h>
#include <ff.h>
#include <lwipthread.h>
#include <ffthread.h>

#endif /* _PLATFORM_H_ */

/******************************** End of file ********************************/

