set(FATFSSRC 
    ${FATFS}/ff.c
    ${FATFS}/ffunicode.c
    ${FATFS}/ffthread.c
    )
set(FATFSINC ${FATFS}/)

set(ALLCSRC ${ALLCSRC} ${FATFSSRC})
set(ALLINC ${ALLINC} ${FATFSINC})
