/******************************************************************************
 * File:        ffthread.h
 * Author:      captain 
 * Date:        18.04.2020
 * Description: Module description
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#ifndef _FFTHREAD_H_
#define _FFTHREAD_H_

#include "ch.h"
#include "hal.h"
#include "ff.h"

#define FATFS_THREAD_STACK_SIZE     (128)
#define FATFS_ATTACH_PERIOD_MS      (500)

enum {
    FATFS_EVT_ATTACH_ERROR  = 1,
    FATFS_EVT_ATTACHED      = 2,
    FATFS_EVT_DEATTACHED    = 4
};

#ifdef __cplusplus
extern "C" {
#endif

bool is_sdcard_mounted(void);
event_source_t * sdcard_event_source(void);
void fatfsInit(void);

#ifdef __cplusplus
}
#endif

#endif /* _FFTHREAD_H_ */

/******************************** End of file ********************************/

