/******************************************************************************
 * File:        ffthread.c
 * Author:      captain 
 * Date:        18.04.2020
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#include "ffthread.h"

static EVENTSOURCE_DECL(fatfs_event_src);
static thread_reference_t ff_trp = NULL;
static bool mounted = false;

static THD_WORKING_AREA(wa_ff_thread, FATFS_THREAD_STACK_SIZE);
static THD_FUNCTION(ff_thread, arg)
{
    (void) arg;

    static FATFS ffs;

    chRegSetThreadName("fatfs");
    sdcStart(&SDCD1, NULL);

    chThdResume(&ff_trp, MSG_OK);
    while (true) {
        chThdSleepMilliseconds(FATFS_ATTACH_PERIOD_MS);
        if (mounted) {
            if (!blkIsInserted(&SDCD1)) {
                mounted = false;
                sdcDisconnect(&SDCD1);
                f_mount(NULL, "", 0);
                chEvtBroadcastFlags(&fatfs_event_src, 
                                    FATFS_EVT_DEATTACHED);
            }
        }
        else {
            if (blkIsInserted(&SDCD1)) {
                if (HAL_SUCCESS == sdcConnect(&SDCD1)) {
                    f_mount(&ffs, "", 0);
                    mounted = true;
                    chEvtBroadcastFlags(&fatfs_event_src, 
                                        FATFS_EVT_ATTACHED);
                }
                else {
                    chEvtBroadcastFlags(&fatfs_event_src, 
                                        FATFS_EVT_ATTACH_ERROR);
                }
            }
        }
    }
}

bool is_sdcard_mounted(void)
{
    return mounted;
}

event_source_t * sdcard_event_source(void)
{
    return &fatfs_event_src;
}

void fatfsInit(void)
{
    chThdCreateStatic(  wa_ff_thread, sizeof(wa_ff_thread),
                        LOWPRIO, ff_thread, NULL );
    chSysLock();
    chThdSuspendS(&ff_trp);
    chSysUnlock();
}

/******************************** End of file ********************************/

