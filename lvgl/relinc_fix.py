#!/usr/bin/env python3
# -*- conding: utf-8 -*-

""" relinc_fix

Module descriprion.
"""

# imports

__author__      = "captain"
__email__       = "pvbmstu@gmail.com"
__copyright__   = "Copyright (c) 2020 captain"
__license__     = "BSD"

import pathlib
import re

def main():
    p = pathlib.Path(".").rglob("*.[ch]")
    for pp in p:
        with open(pp) as f:
            d = []
            n = 0
            for ln in f.readlines():
                lln, nn = re.subn(r"#include \"(.+/)+", "#include \"", ln)
                if nn: print("{}: {} -> {}".format(pp, ln[:-1], lln[:-1]))
                n += nn
                d.append(lln)

        if n:
            with open(pp, "w") as f:
                f.writelines(d)

if __name__ == "__main__":
    main()

