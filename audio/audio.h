/******************************************************************************
 * File:        audio.h
 * Author:      captain 
 * Date:        28.04.2020
 * Description: Module description
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#ifndef _AUDIO_H_
#define _AUDIO_H_

#include "sai.h"

#define ACH_LEFT(smp)           (((int16_t) (smp >> 16)))
#define ACH_RIGHT(smp)          (((int16_t) (smp & 0xffff)))
#define ACH_SAMPLE(left, right) ((audio_sample_t) (right | (left << 16)))

enum {
    hp_out = 0,
    spk_out = 1,
    line_in = 2,
    mic_in = 3
};

typedef union {
    uint32_t sample;
    struct {
        int16_t left;
        int16_t right;
    };
} audio_sample_t;

typedef struct {
    event_source_t * es;
    event_listener_t el;
    eventid_t eid;
    int ptr;
} audio_stream_t;

#ifdef __cplusplus
extern "C" {
#endif

void audio_init(void);
int audio_stream_open(audio_stream_t * st, int stream_id);
void audio_stream_close(audio_stream_t * st);
audio_sample_t audio_get(audio_stream_t * st);
void audio_put(audio_stream_t * st, audio_sample_t smp);

#ifdef __cplusplus
}
#endif

#endif /* _AUDIO_H_ */

/******************************** End of file ********************************/

