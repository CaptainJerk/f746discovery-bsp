/******************************************************************************
 * File:        audio.c
 * Author:      captain 
 * Date:        22.04.2020
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#include "wm8994.h"
#include "audio.h"

/** 
 * @defgroup audio Audio module
 * @{
 */

/**
 * @brief   Audio buffer size in samples. 
 *          Each sample consists of 2 slots and each slot - of 2 channels.
 *          Must be power of 2.
 */
#define AUDIO_BUFFER_SIZE 64

static event_source_t ies, oes;
static int16_t * ib;
static int16_t * ob;
static bool hp_out_lock = false;
static bool spk_out_lock = false;

static void audio_in_callback(void * said, bool ready)
{
    (void) said;
    (void) ready;

    osalSysLockFromISR();
    osalEventBroadcastFlagsI(&ies, 0);
    osalSysUnlockFromISR();
}

static void audio_out_callback(void * said, bool ready)
{
    (void) said;
    (void) ready;

    osalSysLockFromISR();
    osalEventBroadcastFlagsI(&oes, 0);
    osalSysUnlockFromISR();
}

/**
 * @brief   Initialize audio stream device.
 */
void audio_init(void)
{
    static const SAIConfig sai_in_cfg = {
        .cr1    =   SAI_CR1_MCKDIV(4) | SAI_CR1_CKSTR |             \
                    SAI_CR1_DATASIZE_16 |                           \
                    SAI_CR1_MODE_SLAVE_RX | SAI_CR1_SYNC_INTERNAL,
        .slotr  =   SAI_SLOTR_SLOT_NUM(4) |                         \
                    SAI_SLOTR_ENABLE(0) | SAI_SLOTR_ENABLE(2) |     \
                    SAI_SLOTR_ENABLE(1) | SAI_SLOTR_ENABLE(3) |     \
                    SAI_SLOT_SIZE_DS,
        .frcr   =   SAI_FRCR_FSOFF | SAI_FRCR_FSDEF |               \
                    SAI_FRCR_ACTIVE_LEN(32) |
                    SAI_FRCR_TOTAL_LEN(64),
        .circular = true,
        .func =     audio_in_callback
    };

    static const SAIConfig sai_out_cfg = {
        .cr1    =   SAI_CR1_MCKDIV(4) | SAI_CR1_CKSTR |             \
                    SAI_CR1_DATASIZE_16 |                           \
                    SAI_CR1_MODE_MASTER_TX,
        .slotr  =   SAI_SLOTR_SLOT_NUM(4) |                         \
                    SAI_SLOTR_ENABLE(0) | SAI_SLOTR_ENABLE(2) |     \
                    SAI_SLOTR_ENABLE(1) | SAI_SLOTR_ENABLE(3) |     \
                    SAI_SLOT_SIZE_DS,
        .frcr   =   SAI_FRCR_FSOFF | SAI_FRCR_FSDEF |               \
                    SAI_FRCR_ACTIVE_LEN(32) |
                    SAI_FRCR_TOTAL_LEN(64),
        .circular = true,
        .func =     audio_out_callback
    };

    /* Allocate buffers for audio data. */
    ib = chHeapAlloc(NULL, AUDIO_BUFFER_SIZE * sizeof(int16_t) * 4);
    ob = chHeapAlloc(NULL, AUDIO_BUFFER_SIZE * sizeof(int16_t) * 4);
    osalDbgAssert(ib && ob, "allocation fail");

    /* SAI transfer events. */
    osalEventObjectInit(&ies);
    osalEventObjectInit(&oes);

    /* Initialize SAI driver. */
    saiInit();

    /* Start SAI peripheral. */
    saiStart(&SAID2A, &sai_out_cfg);
    saiStart(&SAID2B, &sai_in_cfg);

    /* Initialize wm8994 audio codec.
     * The volume should not exceed 70 as any value above causes
     * line in / headphones crosstalk. */
    wm8994_Init(0x0, 
                OUTPUT_DEVICE_BOTH | INPUT_DEVICE_BOTH, 
                70, AUDIO_FREQUENCY_48K);

    /* Start audio data transfers. */
    osalSysLock();
    saiStartExchangeI(&SAID2A, ob, AUDIO_BUFFER_SIZE * 4);
    saiStartExchangeI(&SAID2B, ib, AUDIO_BUFFER_SIZE * 4);
    osalSysUnlock();
}

/**
 * @brief   Open audio stream.
 *          Opens audio stream if possible. Input stream can be opened
 *          multiple times, output streams can be opened just once.
 * @param   st          audio_stream object pointer.
 * @param   stream_id   Event id to bind audio stream notifications to.
 *
 * @return  0 - on success, -1 - if error occured.
 */
int audio_stream_open(audio_stream_t * st, int stream_id)
{
    osalSysLock();
    if (stream_id == hp_out) {
        if (hp_out_lock) goto st_open_error;
        hp_out_lock = true;
        st->es = &oes;
    }
    else if (stream_id == spk_out) {
        if (spk_out_lock) goto st_open_error;
        spk_out_lock = true;
        st->es = &oes;
    }
    else if (stream_id == line_in || stream_id == mic_in) {
        st->es = &ies;
    }
    else goto st_open_error;
    osalSysUnlock();

    st->eid = stream_id;
    st->ptr = 0;
    chEvtRegister(st->es, &st->el, stream_id);

    return 0;

st_open_error:
    osalSysUnlock();
    return -1;
}

/**
 * @brief   Close audio stream.
 * @param st audio_stream object pointer.
 */
void audio_stream_close(audio_stream_t * st)
{
    if (st->eid == hp_out) hp_out_lock = false;
    else if (st->eid == spk_out) spk_out_lock = false;
    chEvtUnregister(st->es, &st->el);
}

/**
 * @brief   Get audio sample from the input audio stream.
 *          The stream should be opened prior to the call.
 * @note    The function should only be called from within
 *          thread context.
 * @param   st  audio_stream object pointer.
 */
audio_sample_t audio_get(audio_stream_t * st)
{
    /* Wait on every half buffer for notification. */
    if (st->ptr == 0 || st->ptr == AUDIO_BUFFER_SIZE / 2) {
        chEvtWaitOne(EVENT_MASK(st->eid));
    }

    audio_sample_t ret = {0};

    switch (st->eid) {
        case line_in:
            ret.left = ib[st->ptr * 4];
            ret.right = ib[st->ptr * 4 + 2];
            break;
        case mic_in:
            /* Digital mix channels are reversed. */
            ret.right = ib[st->ptr * 4 + 1];
            ret.left = ib[st->ptr * 4 + 3];
            break;
        default: return ret;
    }

    st->ptr = (AUDIO_BUFFER_SIZE - 1) & (st->ptr + 1);

    return ret;
}

/**
 * @brief   Put audio sample into the output audio stream.
 *          The stream should be opened prior to the call.
 * @note    The function should only be called from within
 *          thread context.
 * @param   st  audio_stream object pointer.
 * @param   smp audio sample to put.
 */
void audio_put(audio_stream_t * st, audio_sample_t smp)
{
    if (st->ptr == 0 || st->ptr == AUDIO_BUFFER_SIZE / 2) {
        chEvtWaitOne(EVENT_MASK(st->eid));
    }

    switch (st->eid) {
        case hp_out:
            ob[st->ptr * 4] = smp.left;
            ob[st->ptr * 4 + 2] = smp.right;
            break;
        case spk_out:
            ob[st->ptr * 4 + 1] = smp.left;
            ob[st->ptr * 4 + 3] = smp.right;
            break;
        default: return;
    }

    st->ptr = (AUDIO_BUFFER_SIZE - 1) & (st->ptr + 1);
}

/**
 * @}
 */

/******************************** End of file ********************************/

