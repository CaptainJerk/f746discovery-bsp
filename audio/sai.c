/******************************************************************************
 * File:        sai.c
 * Author:      captain 
 * Date:        26.04.2020
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#include "sai.h"

/**
 * @defgroup SAI SAI Driver
 * @{
 */

#if STM32_SAI_USE_SAI1A
SAIDriver SAID1A;
#endif

#if STM32_SAI_USE_SAI1B
SAIDriver SAID1B;
#endif

#if STM32_SAI_USE_SAI2A
SAIDriver SAID2A;
#endif

#if STM32_SAI_USE_SAI2B
SAIDriver SAID2B;
#endif

#define STM32_SAI1A_DMA_CHN  0x00000000
#define STM32_SAI1B_DMA_CHN  0x00010000
#define STM32_SAI2A_DMA_CHN  0x00030000
#define STM32_SAI2B_DMA_CHN  0x03000000

void sai_dma_isr_handler(void * arg, uint32_t flags)
{
    SAIDriver * saidp = arg;
    if (flags & STM32_DMA_ISR_TEIF) {
        saidp->state = SAI_ERROR;
    }
    else if (flags & STM32_DMA_ISR_TCIF) {
        saidp->state = saidp->cfg->circular ? SAI_ACTIVE : SAI_READY;
        saidp->cfg->func(arg, true);
    }
    else if (flags & STM32_DMA_ISR_HTIF) {
        saidp->cfg->func(arg, false);
    }
}

/**
 * @brief   Initialize SAI module.
 */
void saiInit(void)
{

#if STM32_SAI_USE_SAI1A || STM32_SAI_USE_SAI1B
    /* Enable SAI1 clock */
    RCC->APB2ENR |= RCC_APB2ENR_SAI1EN;

#if STM32_SAI_SAI1_SYNCIN
    SAI1->GCR = STM32_SAI_SAI1_SYNCOUT | SAI_GCR_SYNCIN_1;
#else
    SAI1->GCR = STM32_SAI_SAI1_SYNCOUT;
#endif

#endif

#if STM32_SAI_USE_SAI2A || STM32_SAI_USE_SAI2B
    /* Enable SAI2 clock */
    RCC->APB2ENR |= RCC_APB2ENR_SAI2EN;

#if STM32_SAI_SAI1_SYNCIN
    SAI2->GCR = STM32_SAI_SAI2_SYNCOUT | SAI_GCR_SYNCIN_0;
#else
    SAI2->GCR = STM32_SAI_SAI2_SYNCOUT;
#endif

#endif

#if STM32_SAI_USE_SAI1A
    SAID1A.sai = SAI1_Block_A;
    SAID1A.dma = dmaStreamAlloc(STM32_SAI_SAI1A_DMA_STREAM,
                                STM32_SAI_SAI1A_DMA_PRIORITY,
                                sai_dma_isr_handler, &SAID1A);
    SAID1A.dmamode =    STM32_DMA_CR_MINC | STM32_DMA_CR_MSIZE_WORD |
                        STM32_DMA_CR_CHSEL(STM32_DMA_GETCHANNEL(
                                        STM32_SAI_SAI1A_DMA_STREAM, 
                                        STM32_SAI1A_DMA_CHN )) |
                        STM32_DMA_CR_TEIE |
                        STM32_DMA_CR_PL(1);
    SAID1A.state = SAI_STOP;
    dmaStreamSetFIFO(SAID1A.dma, STM32_DMA_FCR_FTH_HALF | STM32_DMA_FCR_DMDIS);
#endif

#if STM32_SAI_USE_SAI1B
    SAID1B.sai = SAI1_Block_B;
    SAID1B.dma = dmaStreamAlloc(STM32_SAI_SAI1B_DMA_STREAM,
                                STM32_SAI_SAI1B_DMA_PRIORITY,
                                sai_dma_isr_handler, &SAID1B);
    SAID1B.dmamode =    STM32_DMA_CR_MINC | STM32_DMA_CR_MSIZE_WORD |
                        STM32_DMA_CR_CHSEL(STM32_DMA_GETCHANNEL(
                                            STM32_SAI_SAI1B_DMA_STREAM, 
                                            STM32_SAI1B_DMA_CHN )) |
                        STM32_DMA_CR_TEIE |
                        STM32_DMA_CR_PL(1);
    SAID1B.state = SAI_STOP;
    dmaStreamSetFIFO(SAID1B.dma, STM32_DMA_FCR_FTH_HALF | STM32_DMA_FCR_DMDIS);
#endif

#if STM32_SAI_USE_SAI2A
    SAID2A.sai = SAI2_Block_A;
    SAID2A.dma = dmaStreamAlloc(STM32_SAI_SAI2A_DMA_STREAM,
                                STM32_SAI_SAI2A_DMA_PRIORITY,
                                sai_dma_isr_handler, &SAID2A);
    SAID2A.dmamode =    STM32_DMA_CR_MINC | STM32_DMA_CR_MSIZE_WORD |
                        STM32_DMA_CR_CHSEL(STM32_DMA_GETCHANNEL(
                                            STM32_SAI_SAI2A_DMA_STREAM, 
                                            STM32_SAI2A_DMA_CHN )) |
                        STM32_DMA_CR_TEIE |
                        STM32_DMA_CR_PL(1);
    SAID2A.state = SAI_STOP;
    dmaStreamSetFIFO(SAID2A.dma, STM32_DMA_FCR_FTH_HALF | STM32_DMA_FCR_DMDIS);
#endif

#if STM32_SAI_USE_SAI2B
    SAID2B.sai = SAI2_Block_B;
    SAID2B.dma = dmaStreamAlloc(STM32_SAI_SAI2B_DMA_STREAM, 
                                STM32_SAI_SAI2B_DMA_PRIORITY, 
                                sai_dma_isr_handler, &SAID2B);
    SAID2B.dmamode =    STM32_DMA_CR_MINC | STM32_DMA_CR_MSIZE_WORD |
                        STM32_DMA_CR_CHSEL(STM32_DMA_GETCHANNEL(
                                            STM32_SAI_SAI2B_DMA_STREAM, 
                                            STM32_SAI2B_DMA_CHN )) |
                        STM32_DMA_CR_TEIE |
                        STM32_DMA_CR_PL(1);

    SAID2B.state = SAI_STOP;
    dmaStreamSetFIFO(SAID2B.dma, STM32_DMA_FCR_FTH_HALF | STM32_DMA_FCR_DMDIS);
#endif

}

/**
 * @brief   Start the SAI peripheral.
 * @param saidp SAI driver pointer.
 * @param cfg   SAI config.
 */
void saiStart(SAIDriver * saidp, const SAIConfig * cfg)
{
    if (SAI_STOP == saidp->state) {

        saidp->sai->CR1 = cfg->cr1 & ~(SAI_xCR1_SAIEN | SAI_xCR1_DMAEN);
        saidp->sai->CR2 = (cfg->cr2 & ~SAI_xCR2_FTH_Msk) | SAI_xCR2_FTH_1;
        saidp->sai->FRCR = cfg->frcr;
        saidp->sai->SLOTR = cfg->slotr;
        saidp->cfg = cfg;

        uint32_t dmamode = saidp->dmamode;
        
        /* Guess DMA direction from SAI CR1 register value. */
        switch (cfg->cr1 & SAI_xCR1_MODE_Msk) {
            case SAI_CR1_MODE_SLAVE_RX:
            case SAI_CR1_MODE_MASTER_RX:
                dmamode |= STM32_DMA_CR_DIR_P2M;
                break;
            default:
                dmamode |= STM32_DMA_CR_DIR_M2P;
        }
        
        /* Guess DMA data size. */
        switch (cfg->cr1 & SAI_xCR1_DS_Msk) {
            case SAI_CR1_DATASIZE_8:
                dmamode |= STM32_DMA_CR_PSIZE_BYTE;
                break;
            case SAI_CR1_DATASIZE_10:
            case SAI_CR1_DATASIZE_16:
                dmamode |= STM32_DMA_CR_PSIZE_HWORD;
                break;
            case SAI_CR1_DATASIZE_20:
            case SAI_CR1_DATASIZE_24:
            case SAI_CR1_DATASIZE_32:
                dmamode |= STM32_DMA_CR_PSIZE_WORD;
                break;
            default:
                osalDbgAssert(false, "invalid size");
        }

        /* Circular mode. */
        if (cfg->circular) dmamode |= STM32_DMA_CR_CIRC;
        if (cfg->func) dmamode |= STM32_DMA_CR_TCIE | STM32_DMA_CR_HTIE;

        /* Set DMA stream configuration. */
        dmaStreamSetMode(saidp->dma, dmamode);

        /* Enable the peripheral. */
        saidp->sai->CR1 |= SAI_xCR1_SAIEN;
        saidp->state = SAI_READY;
    }
}

/**
 * @brief   Stop the peripheral.
 *          Stop the peripheral and any ongoing transfer.
 * @param saidp SAI driver pointer.
 */
void saiStop(SAIDriver * saidp)
{
    osalSysLock();
    saiStopExchangeI(saidp);
    osalSysUnlock();

    saidp->sai->CR1 &= ~SAI_xCR1_SAIEN;
    saidp->state = SAI_STOP;
}

/**
 * @brief   Start SAI exchange.
 *          If SAI is configured as TX - start audio data output,
 *          if SAI is configured as RX - start audio sample reception.
 * @param saidp SAI driver pointer.
 * @param buf   Buffer to store the samples.
 * @param size  Size of buffer. This parameter is determined by SAI data width.
 *              For more information - refere to DMA transfer size (ref. man.).
 */
void saiStartExchangeI( SAIDriver * saidp, 
                        void * buf, 
                        size_t size )
{
    /* Only start transfer if the driver is ready. */
    if (SAI_READY == saidp->state) {
        saidp->buf = buf;
        saidp->size = size;
        saidp->state = SAI_ACTIVE;
        dmaStreamSetPeripheral(saidp->dma, &(saidp->sai->DR));
        dmaStreamSetMemory0(saidp->dma, buf);
        dmaStreamSetTransactionSize(saidp->dma, size);
        dmaStreamEnable(saidp->dma);
        saidp->sai->CR1 |= SAI_xCR1_DMAEN;
    }
}

/**
 * @brief   Stop SAI exchange.
 * @param saidp SAI driver pointer.
 */
void saiStopExchangeI(SAIDriver * saidp)
{
    if (SAI_ACTIVE == saidp->state) {
        saidp->sai->CR1 &= ~SAI_xCR1_DMAEN;
        dmaStreamDisable(saidp->dma);
        saidp->sai->CR2 |= SAI_xCR2_FFLUSH;
        saidp->state = SAI_READY;
    }
}

/**
 * @}
 */

/******************************** End of file ********************************/

