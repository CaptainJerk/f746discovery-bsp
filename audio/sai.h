/******************************************************************************
 * File:        sai.h
 * Author:      captain 
 * Date:        26.04.2020
 * Description: Module description
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#ifndef _SAI_H_
#define _SAI_H_

#include "hal.h"

/**
 * @addtogroup SAI
 * @{
 */

#define SAI_GCR_NO_SYNCOUT              (0)
#define SAI_GCR_SYNCOUT_A               (SAI_GCR_SYNCOUT_0)
#define SAI_GCR_SYNCOUT_B               (SAI_GCR_SYNCOUT_1)

/**
 * @name   SAI xCR1 register
 * @{
 */
#define SAI_CR1_MCKDIV(d)               (((d == 1) ? 0 : d / 2) << SAI_xCR1_MCKDIV_Pos)
#define SAI_CR1_NODIV                   SAI_xCR1_NODIV
#define SAI_CR1_OUTDRIV                 SAI_xCR1_OUTDRIV
#define SAI_CR1_MONO                    SAI_xCR1_MONO
#define SAI_CR1_SYNC_INTERNAL           SAI_xCR1_SYNCEN_0
#define SAI_CR1_SYNC_EXTERNAL           SAI_xCR1_SYNCEN_1
#define SAI_CR1_CKSTR                   SAI_xCR1_CKSTR
#define SAI_CR1_LSBFIRST                SAI_xCR1_LSBFIRST
#define SAI_CR1_DATASIZE_8              SAI_xCR1_DS_1
#define SAI_CR1_DATASIZE_10             (SAI_xCR1_DS_1 | SAI_xCR1_DS_0)
#define SAI_CR1_DATASIZE_16             SAI_xCR1_DS_2
#define SAI_CR1_DATASIZE_20             (SAI_xCR1_DS_2 | SAI_xCR1_DS_0)
#define SAI_CR1_DATASIZE_24             (SAI_xCR1_DS_1 | SAI_xCR1_DS_2 )
#define SAI_CR1_DATASIZE_32             (SAI_xCR1_DS_0 | SAI_xCR1_DS_1 \
                                        | SAI_xCR1_DS_2)
#define SAI_CR1_PROT_SPDIF              (SAI_xCR1_PRTCFG_0)
#define SAI_CR1_PROT_AC97               (SAI_xCR1_PRTCFG_1)
#define SAI_CR1_MODE_MASTER_TX          (0)
#define SAI_CR1_MODE_MASTER_RX          (SAI_xCR1_MODE_0)
#define SAI_CR1_MODE_SLAVE_TX           (SAI_xCR1_MODE_1)
#define SAI_CR1_MODE_SLAVE_RX           (SAI_xCR1_MODE_0 | SAI_xCR1_MODE_1)
/** @} */

/**
 * @name   SAI CR2 register
 * @{
 */
#define SAI_CR2_COMP_ULAW               (SAI_xCR2_COMP_1)
#define SAI_CR2_COMP_ALAW               (SAI_xCR2_COMP_1 | SAI_xCR2_COMP_0)
#define SAI_CR2_CPL                     (SAI_xCR2_CPL)
/** @} */

/**
 * @name   SAI xFRCR register
 * @{
 */
#define SAI_FRCR_FSOFF                  (SAI_xFRCR_FSOFF)
#define SAI_FRCR_FSPOL                  (SAI_xFRCR_FSPOL)
#define SAI_FRCR_FSDEF                  (SAI_xFRCR_FSDEF)
#define SAI_FRCR_ACTIVE_LEN(n)          ((n - 1) << SAI_xFRCR_FSALL_Pos)
#define SAI_FRCR_TOTAL_LEN(n)           ((n - 1) << SAI_xFRCR_FRL_Pos)
/** @} */

/**
 * @name   SAI xSLOTR register
 * @{
 */
#define SAI_SLOTR_ENABLE(n)             ((1 << n) << SAI_xSLOTR_SLOTEN_Pos)
#define SAI_SLOTR_SLOT_NUM(n)           ((n - 1) << SAI_xSLOTR_NBSLOT_Pos)
#define SAI_SLOTR_FBOFF(n)              (n << SAI_xSLOTR_FBOFF_Pos)
#define SAI_SLOT_SIZE_DS                (0)
#define SAI_SLOT_SIZE_HWORD             (SAI_xSLOTR_SLOTSZ_0)
#define SAI_SLOT_SIZE_WORD              (SAI_xSLOTR_SLOTSZ_1)
/** @} */

/** 
 * @name    Configuration options
 * @{
 */
#ifndef STM32_SAI_USE_SAI1A
#define STM32_SAI_USE_SAI1A             FALSE
#endif

#ifndef STM32_SAI_USE_SAI1B
#define STM32_SAI_USE_SAI1B             FALSE
#endif

#ifndef STM32_SAI_USE_SAI2A
#define STM32_SAI_USE_SAI2A             FALSE
#endif

#ifndef STM32_SAI_USE_SAI2B
#define STM32_SAI_USE_SAI2B             FALSE
#endif

#ifndef STM32_SAI_SAI1_SYNCOUT
#define STM32_SAI_SAI1_SYNCOUT          SAI_GCR_NO_SYNCOUT
#endif

#ifndef STM32_SAI_SAI1_SYNCIN
#define STM32_SAI_SAI1_SYNCIN           FALSE
#endif

#ifndef STM32_SAI_SAI2_SYNCOUT
#define STM32_SAI_SAI2_SYNCOUT          SAI_GCR_NO_SYNCOUT
#endif

#ifndef STM32_SAI_SAI2_SYNCIN
#define STM32_SAI_SAI2_SYNCIN           FALSE
#endif

#ifndef STM32_SAI_SAI1A_DMA_STREAM
#define STM32_SAI_SAI1A_DMA_STREAM      STM32_DMA_STREAM_ID(2, 1)
//#define STM32_SAI_SAI1A_DMA_STREAM      STM32_DMA_STREAM_ID(2, 3)
#endif

#ifndef STM32_SAI_SAI1B_DMA_STREAM
#define STM32_SAI_SAI1B_DMA_STREAM      STM32_DMA_STREAM_ID(2, 4)
//#define STM32_SAI_SAI1B_DMA_STREAM      STM32_DMA_STREAM_ID(2, 5)
#endif

#ifndef STM32_SAI_SAI2A_DMA_STREAM
#define STM32_SAI_SAI2A_DMA_STREAM      STM32_DMA_STREAM_ID(2, 4)
#endif

#ifndef STM32_SAI_SAI2B_DMA_STREAM
#define STM32_SAI_SAI2B_DMA_STREAM      STM32_DMA_STREAM_ID(2, 6)
//#define STM32_SAI_SAI2B_DMA_STREAM      STM32_DMA_STREAM_ID(2, 7)
#endif

#ifndef STM32_SAI_SAI1A_DMA_PRIORITY
#define STM32_SAI_SAI1A_DMA_PRIORITY    3
#endif

#ifndef STM32_SAI_SAI1B_DMA_PRIORITY
#define STM32_SAI_SAI1B_DMA_PRIORITY    3
#endif

#ifndef STM32_SAI_SAI2A_DMA_PRIORITY
#define STM32_SAI_SAI2A_DMA_PRIORITY    3
#endif

#ifndef STM32_SAI_SAI2B_DMA_PRIORITY
#define STM32_SAI_SAI2B_DMA_PRIORITY    3
#endif

/**
 * @}
 */

typedef enum {
    SAI_UNINIT = 0,
    SAI_STOP,
    SAI_ACTIVE,
    SAI_READY,
    SAI_ERROR
} sai_state_t;

/**
 * @brief SAI config structure.
 */
typedef struct {
    /// Circular mode.
    bool circular;
    /// Transfer half/complete callback.
    void (*func)(void *, bool);
    /// SAI CR1.
    uint32_t cr1;
    /// SAI CR2.
    uint32_t cr2;
    /// SAI FRCR.
    uint32_t frcr;
    /// SAI SLOTR.
    uint32_t slotr;
} SAIConfig;

typedef struct sai_driver_s {
    SAI_Block_TypeDef * sai;
    const stm32_dma_stream_t * dma;
    sai_state_t state;
    uint32_t dmamode;
    void * buf;
    size_t size;
    const SAIConfig * cfg;
} SAIDriver;

/**
 * @}
 */

#if STM32_SAI_USE_SAI1A
extern SAIDriver SAID1A;
#endif

#if STM32_SAI_USE_SAI1B
extern SAIDriver SAID1B;
#endif

#if STM32_SAI_USE_SAI2A
extern SAIDriver SAID2A;
#endif

#if STM32_SAI_USE_SAI2B
extern SAIDriver SAID2B;
#endif

#ifdef __cplusplus
extern "C" {
#endif

void saiInit(void);
void saiStart(SAIDriver * saidp, const SAIConfig * cfg);
void saiStartExchangeI( SAIDriver * saidp, 
                        void * buf, 
                        size_t size );
void saiStopExchangeI(SAIDriver * saidp);
void saiStop(SAIDriver * saidp);

#ifdef __cplusplus
}
#endif

#endif /* _SAI_H_ */

/******************************** End of file ********************************/

