/******************************************************************************
 * File:        ft5x06.c
 * Author:      captain 
 * Date:        15.04.2020
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#include "ft5x06.h"
#include "hal.h"
#include <string.h>

/**
 * @defgroup ft5x06 FT5x06 CTP driver
 * @{
 */

#define FT5x06_ADR              0x70
#define FT5x06_REGMAP_OFFSET    0x03
#define FT5x06_COORD(xh, xl)    (((xh & 0xf) << 8) | xl)
#define FT5x06_EVENT(xh)        (xh >> 6)

struct regs_ch_s {
    uint8_t TOUCH_XH;
    uint8_t TOUCH_XL;
    uint8_t TOUCH_YH;
    uint8_t TOUCH_YL;
    uint8_t __dummy__[2];
};

struct ft5x06_drv_s {
    void * bus;
    struct regs_ch_s rm[5];
};

/**
 * @brief Initialize drive object.
 * Usage:
 *  static FT5x06_DRV_OBJECT(pool);
 *  ft5x06_drv_t * drv = ft5x06_object_init(pool, &I2CDx);
 *  
 * @param pool  Pool data
 * @param bus   I2C bus driver
 *
 * @return      CTP driver instance
 */
ft5x06_drv_t * ft5x06_object_init(void * pool, void * bus)
{
    chDbgAssert(pool, "invalid channel");

    ft5x06_drv_t * ret = pool;
    ret->bus = bus;
    memset(&ret->rm, 0, sizeof(ret->rm));

    return ret;
}

/**
 * @brief Start and initialize the CTP device.
 *
 * @param drv   CTP driver instance
 * @param cfg   NULL
 *
 * @return 0 if succeeded; -1 - else
 */
int ft5x06_start(ft5x06_drv_t * drv, void * cfg)
{
    (void) drv;
    (void) cfg;
    return 0;
}

/**
 * @brief Update cached touch data.
 * @param drv   CTP driver instance
 *
 * @return 0 if succeeded; -1 - else
 */
int ft5x06_update(ft5x06_drv_t * drv)
{
    const uint8_t req = FT5x06_REGMAP_OFFSET;
    i2cAcquireBus(drv->bus);
    msg_t ret = i2cMasterTransmitTimeout(   drv->bus, FT5x06_ADR >> 1, 
                                            &req, sizeof(req), 
                                            (void *) drv->rm, 
                                            sizeof(drv->rm), 
                                            TIME_MS2I(50) );
    i2cReleaseBus(drv->bus);
    if (MSG_OK != ret) return -1;
    return 0;
}

/**
 * @brief Get touch panel state.
 * Get the touch panel state. Call ft5x06_update prior to this.
 * @param drv   CTP driver instance
 * @param ch    channel [0..4]
 * @param x     returned x touch coordinate
 * @param y     returned y touch coordinate
 *
 * @return      Touch event flag as per datasheet
 */
uint8_t ft5x06_get_channel( ft5x06_drv_t * drv, uint8_t ch, 
                            short * x, short * y)
{
    chDbgAssert(ch < 5, "invalid channel");
    struct regs_ch_s * c = &drv->rm[ch];

    *x = FT5x06_COORD(c->TOUCH_XH, c->TOUCH_XL);
    *y = FT5x06_COORD(c->TOUCH_YH, c->TOUCH_YL);

    return FT5x06_EVENT(c->TOUCH_XH);
}

/**
 * @}
 */

/******************************** End of file ********************************/

