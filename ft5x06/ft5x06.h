/******************************************************************************
 * File:        ft5x06.h
 * Author:      captain 
 * Date:        15.04.2020
 * Description: Module description
 *              
 ******************************************************************************
 * Copyright (c) 2020 captain
 * All rights reserved.
 * 
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/

#ifndef _FT5X06_H_
#define _FT5X06_H_

#include <stdbool.h>
#include <stdint.h>

#define FT5x06_DRV_SIZE sizeof(struct { void * _0; uint8_t _1[6 * 5]; })
#define FT5x06_DRV_OBJECT(name) uint32_t name[(FT5x06_DRV_SIZE + 3) / 4]

typedef struct ft5x06_drv_s ft5x06_drv_t;

ft5x06_drv_t * ft5x06_object_init(void * pool, void * bus);
int ft5x06_start(ft5x06_drv_t * drv, void * config);
int ft5x06_update(ft5x06_drv_t * drv);
uint8_t ft5x06_get_channel( ft5x06_drv_t * drv, uint8_t ch, 
                            short * x, short * y );

#endif /* _FT5X06_H_ */

/******************************** End of file ********************************/

